class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :url, null: false
      t.string :hashid, index: true
      t.timestamps null: false

    end
  end
end
