Rails.application.routes.draw do
  resources :addresses
  root 'addresses#new'
  get '/:hashid' => 'addresses#redirect'
end
