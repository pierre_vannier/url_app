class Address < ActiveRecord::Base
  validates :url, :url => true, :length => { :maximum => 500 }
  after_create :generate_hashid

  private
    def generate_hashid
      hashids = Hashids.new("this is my salt")
      self.hashid = hashids.encode(self.id)
      self.save()
    end
end
