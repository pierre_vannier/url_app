class AddressesController < ApplicationController

  def redirect
    @address = Address.find_by(hashid: params[:hashid])
    redirect_to @address.url
  end

  def show
    @address = Address.find(params[:id])
    @url = "#{request.base_url}/#{@address.hashid}"
  end

  def create
    @address = Address.new(address_params)
    @address.save ? redirect_to(@address) : render('new')
  end

  private
  def address_params
    params.require(:address).permit(:url, :id, :hashid)
  end
end
