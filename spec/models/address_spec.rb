require 'rails_helper'

RSpec.describe Address, type: :model do
  it "url should not be empty" do
    new_url = Address.new(url: "")
    expect {new_url.save!}.to  raise_error(ActiveRecord::RecordInvalid,
                                          "Validation failed: Url is not a valid URL")
  end

  it "url should have the correct form" do
    new_url = Address.new(url: "hsdsdq")
    expect {new_url.save!}.to raise_error(ActiveRecord::RecordInvalid,
                                          "Validation failed: Url is not a valid URL")
  end

  it "url should have the correct form test 2" do
    new_url = Address.new(url: "ht:qskjnqs;:q")
    expect {new_url.save!}.to raise_error(ActiveRecord::RecordInvalid,
                                          "Validation failed: Url is not a valid URL")
  end

  it "url should have the correct form test 3" do
    new_url = Address.new(url: "http:sdqdsqd")
    expect {new_url.save!}.to raise_error(ActiveRecord::RecordInvalid,
                                          "Validation failed: Url is not a valid URL")
  end

  it "url should not be too long (limit 500 chars)" do
    new_url = Address.new(url: "http://www.google.fr/sdqdsqdfr/sdqdsqdfr/sdqdsqd\
                                fr/sdqdsqdfr/sdqdsqdfr/sdqdsqdfr/sdqdsqdfr/sdqdsq\
                                dfr/sdqdsqdfr/sdqdsqdfr/sdqdsqdfr/sdqdsqdfr/sdqdsq\
                                dfr/sdqdsqdfr/sdqdsqdfr/sdqdsqdfr/sdqdsqdfr/sdqdsq\
                                dfr/sdqdsqdfr/sdqdsqdfr/sdqdsqdfr/sdqdsqdfr/sdqdsq\
                                dfr/sdqdsqdfr/sdqdsqdfr/sdqdsqdfr/sdqdsqdfr/sdqdsqd\
                                fr/sdqdsqdfr/sdqdsqdfr/sdqdsqdfr/sdqdsqdfr/sdqdsqdfr\
                                /sdqdsqdfr/sdqdsqd")
    expect {new_url.save!}.to raise_error(ActiveRecord::RecordInvalid,
                                          "Validation failed: Url is too long (maximum is 500 characters)")
  end

  
end
