# Url App

This small app allows a user to enter a long url and get in return a shorten url that he can copy/paste in a browser and get redirected to the final longer url.

This app is deployed on a [Heroku dyno](https://r-short.herokuapp.com/)

Tests are done with rspec. I've done unit tests and a controller test. (located respectively in /app/spec/models and /app/spec/controllers).

**This app uses an external gems :**

[**hashid**](https://github.com/peterhellberg/hashids.rb) to create human uniq hash to append to domain name and allow redirection. In effect, it would have been bad to redirect based on real DB id field.

[**validates_url**](https://github.com/perfectline/validates_url) to validate url properly

For more information, email [**Pierre Vannier**](mailto:pierre.vannier@gmail.com)
